package com.example.artur.meteo;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.squareup.picasso.Picasso;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase;


public class LegendActivity extends ActionBarActivity {

    private ImageViewTouch imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legend);


        imageView = (ImageViewTouch) findViewById(R.id.imageView);
        imageView.setDisplayType(ImageViewTouchBase.DisplayType.FIT_TO_SCREEN);

        Picasso.with(this).load(Config.LEGEND_URL).into(imageView);
    }

}
