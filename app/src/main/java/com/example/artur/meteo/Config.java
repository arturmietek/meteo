package com.example.artur.meteo;

/**
 * Created by artur on 30.04.15.
 */
public class Config {

    public static final boolean DEBUG = true;
    public static final String TAG = "Artur";
    public static String LEGEND_URL = "http://www.meteo.pl/um/metco/leg_um_pl_cbase_256.png";

}
