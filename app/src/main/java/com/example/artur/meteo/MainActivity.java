package com.example.artur.meteo;

import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase;

import static com.example.artur.meteo.Utils.log;


public class MainActivity extends ActionBarActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, LocationListener, SwipeRefreshLayout.OnRefreshListener {
    public static final int CONNECTION_FAIL_RESOLUTION_REQUEST_CODE = 101;
    private static final String LOCATION_PROVIDER_DEFAULT = "DEFAULT_LOCATION";
    private ImageViewTouch imageView;
    private SwipeRefreshLayout swipeLayout;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private Location lastLocation;
    private Target target;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageViewTouch) findViewById(R.id.imageView);
        imageView.setDisplayType(ImageViewTouchBase.DisplayType.FIT_TO_SCREEN);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeLayout.setColorScheme(R.color.swiperefresh_color_1, R.color.swiperefresh_color_2,
                R.color.swiperefresh_color_3, R.color.swiperefresh_color_2);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setEnabled(false);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .build();

        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_LOW_POWER)
                .setFastestInterval(10000);


        if (savedInstanceState != null) {
            try {
                savedInstanceState.clear();
                savedInstanceState = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        googleApiClient.connect();

    }

    @Override
    protected void onPause() {
        super.onPause();
        googleApiClient.disconnect();

        Picasso.with(this).cancelRequest(target);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i;
        switch (item.getItemId()) {
            case R.id.action_refresh:
                onRefresh();
                return true;
            case R.id.action_legend:
                i = new Intent(this, LegendActivity.class);
                startActivity(i);
                return true;
            case R.id.action_settings:
                i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void setupMeteogram(Map<String, String> params) {
        log("setupMeteogram");

        if (null == imageView) {
            log("imageView is null");
            return;
        }

        String url = Utils.getModelBaseURL(getBaseContext()) +
                "?ntype=" + params.get("ntype") +
                "&fdate=" + params.get("fdate") +
                "&row=" + params.get("row") +
                "&col=" + params.get("col") +
                "&lang=pl";

        log("generated image url is "+ url);

        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                swipeLayout.setRefreshing(false);

                if (null == imageView) {
                    return;
                }

                if (bitmap.getByteCount() > 0) {
                    log("setting bitmap");
                    BitmapDrawable drawable = new BitmapDrawable(getResources(), bitmap);
                    imageView.setImageDrawable(drawable);

                } else {
                    error(getString(R.string.error_receiving_meteorogram_image));
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                swipeLayout.setRefreshing(false);
                error(getString(R.string.error_receiving_meteorogram_image));
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        Picasso.with(this).load(url).into(target);
    }

    private void useFallbackLocation() {
        if (lastLocation != null) {
            log("lastLocation present, skipping using fallback location");
            return;
        }

        log("using fallback Warsaw location");

        Toast.makeText(MainActivity.this, getString(R.string.error_receiving_location), Toast.LENGTH_LONG).show();

        Location l = new Location(LOCATION_PROVIDER_DEFAULT);
        l.setLatitude(52.21);
        l.setLongitude(21.0);
        handleLocation(l);
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            log("GoogleApi connection failed, hasResolution");
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAIL_RESOLUTION_REQUEST_CODE);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            log("GoogleApi connection failed without resolution " + connectionResult);
            useFallbackLocation();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        log("GoogleApi connected");
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (lastLocation != null) {
            log("using fused location");
            handleLocation(lastLocation);
        } else {
            log("requesting location updates");
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            useFallbackLocation();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        log("GoogleApi connection suspended");
    }

    @Override
    public void onLocationChanged(Location location) {
        log("GoogleApi location changed "+ location);
        handleLocation(location);
    }

    private void handleLocation(Location location) {
        if (null == location) {
            return;
        }

        log("setting lastLocation to " + location);
        lastLocation = location;

        Calendar currentDate = Calendar.getInstance();

        int year = currentDate.get(Calendar.YEAR);
        int month = currentDate.get(Calendar.MONTH) + 1;
        int day = currentDate.get(Calendar.DAY_OF_MONTH);
        int hour = currentDate.get(Calendar.HOUR_OF_DAY);
        int hourSelected = 0;

        if (hour > 12) {
            hourSelected = 6;
        } else if (hour > 18) {
            hourSelected = 12;
        }

//        log("year="+year+",month="+month+",day="+day+",hour="+hour);

        String url = "http://www.meteo.pl/um/php/mgram_search.php" +
                "?NALL=" + location.getLatitude() +
                "&EALL=" + location.getLongitude() +
                "&lang=pl" +
                "&fdate=" + year +
                Utils.numberPadLeft(month, 2) +
                Utils.numberPadLeft(day, 2) +
                Utils.numberPadLeft(hourSelected, 2);


        GetURLTask task = new GetURLTask();
        task.execute(url);


        if (location.getProvider() == LOCATION_PROVIDER_DEFAULT) {
            setTitle(getText(R.string.app_name) + " - Warszawa");

        } else {
            setTitle(getText(R.string.app_name) + " - " + String.format("%.2f", location.getLatitude()) + "x" + String.format("%.2f", location.getLongitude()));

            Geocoder gcd = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addresses.size() > 0) {
                    setTitle(getText(R.string.app_name) + " - " + addresses.get(0).getLocality());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        swipeLayout.setRefreshing(true);

    }



    @Override
    public void onRefresh() {
        handleLocation(lastLocation);
    }

    public void error(String message) {
        swipeLayout.setRefreshing(false);
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
    }


    public class GetURLTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... args) {
            URL obj = null;
            try {
                log("Calling " + args[0]);

                obj = new URL(args[0]);
                URLConnection conn = obj.openConnection();
                HttpURLConnection httpConn = (HttpURLConnection) conn;
                httpConn.setInstanceFollowRedirects(false);
                conn.connect();

                return conn.getHeaderField("Location");

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String url) {
            if (null == url) {
                if (null != MainActivity.this) {
                    MainActivity.this.error(getString(R.string.error_receiving_meteorogram_url));
                }
                return;
            }

            Map<String, String> query_pairs = new LinkedHashMap<String, String>();
            String[] urlParts = url.split("\\?");
            String query = urlParts[1];
            String[] pairs = query.split("&");
            for (String pair : pairs) {
                int idx = pair.indexOf("=");
                try {
                    query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            setupMeteogram(query_pairs);
        }
    }

}
