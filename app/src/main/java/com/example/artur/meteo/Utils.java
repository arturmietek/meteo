package com.example.artur.meteo;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
	

	/**
	 * Loguje na LogCat'a (konsole) podany komunikat. Logi wyświetlane są
	 * jedynie gdy debug = true.
	 * 
	 * @param message
	 *            Komunikat
	 */
	public static void log(String message) {
		log(Config.TAG, message);
	}

	public static void log(String tag, String message) {
		if (Config.DEBUG) {
			if (null == message) {
				message = "null";
			}
			Log.d(tag, message);
		}
	}

	
	public static String getDateFromUnixtime(int seconds) {
	    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	    
	    long miliSeconds = (long)seconds * 1000L;
	    Date d = new Date(miliSeconds);

	    return formatter.format(d);
	} 
	
	public static int getCurrentTimestamp() {
		Long tsLong = System.currentTimeMillis() / 1000;
		return Integer.parseInt(tsLong.toString());
	}
	
	public static String getMysqlDate(int year, int month, int day) {
		month++;
		return year+"-"+(month<10?"0"+month:month)+"-"+(day<10?"0"+day:day);
	}
	
	public static void setTextWithUnit(TextView tv, String value, String units) {
		tv.setText(Html.fromHtml(value +"<small><small> "+units+"</small></small>"));
	}

    public static String numberPadLeft(String s, int n) {
        return String.format("%1$" + n + "s", s).replace(" ", "0");
    }

    public static String numberPadLeft(int s, int n) {
        return numberPadLeft(String.valueOf(s), n);
    }

	public static String getModelBaseURL(Context context) {
		String[] modelsURLs = context.getResources().getStringArray(R.array.modelListValues);
		SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
		return SP.getString("model", modelsURLs[0]);
	}
}
