-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose

-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-optimizationpasses 5
-allowaccessmodification
-dontpreverify

# butterknife
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewInjector { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

# Dagger
#-dontwarn dagger.internal.codegen.**
#-keepattributes *Annotation*
#-keepclassmembers,allowobfuscation class * {
#    @javax.inject.* *;
#    @dagger.* *;
#    <init>();
#}
#-keep class com.example.app.modules.** { *; }
#-keep class **$$ModuleAdapter
#-keep class **$$InjectAdapter
#-keep class **$$StaticInjection
#-keepnames !abstract class coffee.*
#-keepnames class dagger.Lazy

# Crashlytics
-keepattributes SourceFile,LineNumberTable
#-keep class com.crashlytics.** { *; }

# Remove Log.*(...)
-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
}

# Otto
#-keepattributes *Annotation*
#-keepclassmembers class ** {
#    @com.squareup.otto.Subscribe public *;
#    @com.squareup.otto.Produce public *;
#}

# OrmLite
#-keep class com.j256.**
#-keepclassmembers class com.j256.** { *; }
#-keep enum com.j256.**
#-keepclassmembers enum com.j256.** { *; }
#-keep interface com.j256.**
#-keepclassmembers interface com.j256.** { *; }

# Picasso
-dontwarn com.squareup.okhttp.**
